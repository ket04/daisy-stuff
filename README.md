**📱Android 10 Custom Roms**

|                           Rom Name                           | Android Version |                          Developer                           |  Status  |                        Download link                         | Continues | Official | OTA  |
| :----------------------------------------------------------: | :-------------: | :----------------------------------------------------------: | :------: | :----------------------------------------------------------: | :-------: | :------: | :--: |
|                        AOSIP Derpfest                        |      10.0       |                        Tunahan Sahinn                        | UPDATED  | [SourceForge](https://sourceforge.net/projects/tunahan-s-builds/files/daisy-builds/DerpFest/) |     ✔     |    ✔     |  ❌   |
|                         MSM Extended                         |      10.0       | [TogoFire](https://forum.xda-developers.com/member.php?u=4230687) | UPDATED  | [Google Drive](https://drive.google.com/open?id=114RoL6JAQs9p77w7GMXUP4ic4sAgKvX7) |     ❌     |    ❌     |  ❌   |
|                           Pixys OS                           |      10.0       |                         ScaryMen1325                         | Outdated | [Pixy OS](https://drive.google.com/file/d/1hdlfJRyg0zZbIGKsiec-WrkkGj37Ia4N/view?usp=drivesdk) |     ❌     |    ❌     |  ❌   |
| [POSP](https://forum.xda-developers.com/mi-a2-lite/development/9-0-potato-sauce-project-laciachan-t3953159) |      10.0       |                         Lacia ~chan                          | UPDATED  | [SourceForge](https://sourceforge.net/projects/posp/files/daisy/croquette/) |     ✔     |    ✔     |  ✔   |
|                         PE (sakura)                          |      10.0       |                         ScaryMen1325                         | Outdated | [Google Drive](https://drive.google.com/open?id=12HyekcUie94imlro5cZvSQ5Qk_q4CoJ5) |     ❌     |    ❌     |  ❌   |
| [Havoc](https://forum.xda-developers.com/redmi-6-pro/development/4-9-havoc-os-v2-6-t3942681) (Sakura) |      10.0       |                         ScaryMen1325                         | Outdated | [Google Drive](https://drive.google.com/open?id=1aBeAC6EjQqifJtDBOfGrbtx55J2r_xE6) |     ❌     |    ❌     |  ❌   |
|                             AEX                              |      10.0       |                            acras1                            | UPDATED  |          [Telegram](https://t.me/newsmia2lite/256)           |     ✔     |    ❌     |  ❌   |
|                            AOSIP                             |      10.0       |                            opal06                            | UPDATED  | [SourceForge](https://sourceforge.net/projects/aosip-daisy-ota/files/builds/) |     ✔     |    ❌     |  ❌   |
|                         Superior OS                          |      10.0       | [pawelik001](https://forum.xda-developers.com/member.php?u=8419529) | UPDATED  | [Goolge Drive](https://drive.google.com/open?id=1HuBdADxPoecHA7UtAvw_PHtgGYhV7XrG) |     ✔     |    ✔     |  ✔   |
|                          RevengeOS                           |      10.0       |                        Tunahan Sahinn                        | UPDATED  | [SourceForge](https://sourceforge.net/projects/tunahan-s-builds/files/daisy-builds/Revenge-Q/) |     ✔     |    ✔     |  ✔   |
|                         Evolution X                          |      10.0       |                           ZIDAN44                            |    ?     | [SourceForge](https://sourceforge.net/projects/daisy1/files/rom/) |     ❔     |    ❌     |  ❌   |
|                          Cosmic OS                           |      10.0       |                           ZIDAN44                            | Outdated | [SourceForge](https://sourceforge.net/projects/daisy1/files/rom/Cosmic-OS-v5.0-Quasar-daisy-20200124-2324-UNOFFICIAL.zip/download) |     ❌     |    ❌     |  ❌   |
| [MIUI](https://forum.xda-developers.com/mi-a2-lite/development/9-miui-rom-t3960704) |       9.0       |                        Evolution_serg                        | Outdated | [Google Drive](https://drive.google.com/drive/folders/1efZZvSwJ1pCKFgTAWi98CMQbdBdmwT09?usp=sharing) |     ❌     |    ❌     |  ❌   |
|                        LineageOS 17.1                        |      10.0       |                            Bogdan                            |    ?     | [Google Drive](https://drive.google.com/file/d/1yaMJSVFqmNyMDzOiokNtbOQ448C0Omsa/view?usp=sharing) |     ❌     |    ❌     |  ❌   |
| [Pixel Experience and Plus](https://forum.xda-developers.com/mi-a2-lite/development/rom-pixel-experience-plus-t4069995) |      10.0       |                            FDoop                             | UPDATED  | [SourceForge](https://sourceforge.net/projects/fdoops-builds/files/) / [Telegram Channel](https://t.me/mia2litepe) |     ✔     |    ❌     |  ❌   |
| [ArrowOS](https://forum.xda-developers.com/mi-a2-lite/development/rom-arrowos-v10-0-xiaomi-mi-a2-lite-t4072841) |      10.0       | [ganomin](https://forum.xda-developers.com/member.php?u=9850043) | UPDATED  | [Google Drive](https://drive.google.com/drive/folders/1iYCf3K1pgHHPuF2FZLlg1caeptX8Jbtb) |     ✔     |    ❌     |  ❌   |
| [Bliss ROM](https://forum.xda-developers.com/mi-a2-lite/development/10-0-bliss-rom-v12-5-xiaomi-mi-a2-lite-t4072705) |      10.0       | [krzy12](https://forum.xda-developers.com/member.php?u=5938081) | UPDATED  | [Mega](https://mega.nz/#!7NFEECrR!GQawiMXLoAT2xufFqJ5QGjCPxThp2eCOEvuc-ATPyGY) |     ✔     |    ❌     |  ❌   |
| [MSM Xtended XQ](https://forum.xda-developers.com/mi-a2-lite/development/stable-msm-xtended-xq-release-v6-0-t4063845) |      10.0       | [TogoFire](https://forum.xda-developers.com/member.php?u=4230687) | UPDATED  | [Google Drive](https://drive.google.com/file/d/1fE23XX-Sjg5S032OJFoBK27dsrR8Uin2/view) |     ✔     |    ❌     |  ❌   |
| [ExtendedUI](https://forum.xda-developers.com/mi-a2-lite/development/10-0-extendedui-xiaomi-mi-a2-lite-t4070989) |      10.0       | [pawelik001](https://forum.xda-developers.com/member.php?u=8419529) | UPDATED  | [SourceForge](https://sourceforge.net/projects/extendedui/files/daisy/) |     ✔     |    ✔     |  ❔   |
| [CAOS](https://github.com/C-A-O-S/treble_manifest_caos/wiki/CAOS-Project) |      10.0       |                         eternityson                          | UPDATED  | [Github](https://github.com/C-A-O-S/treble_manifest_caos/wiki/CAOS-Project) |     ✔     |    ❌     |  ❌   |
| [Arrow OS](https://forum.xda-developers.com/mi-a2-lite/development/rom-arrowos-v10-0-xiaomi-mi-a2-lite-t4072841) |      10.0       |                           ganomin                            | UPDATED  |  [Mega](https://mega.nz/#F!ud8BTSza!8EihQfOZp5N23-JM7OO2hg)  |     ✔     |    ❌     |  ❌   |
| [Bliss Rom](https://forum.xda-developers.com/mi-a2-lite/development/10-0-bliss-rom-v12-5-xiaomi-mi-a2-lite-t4072705) |      10.0       |                      krzy12/Avengerhood                      | UPDATED  | [sourceForge](https://sourceforge.net/projects/blissroms/files/Q/daisy/Bliss-v12.5-daisy-OFFICIAL-20200330.zip/download) |     ✔     |    ✔     |  ✔   |
|        [Havoc OS](https://t.me/A2LiteOfficial/154755)        |      10.0       |            [LuPeS0lTec](https://t.me/LuPeS0lTec)             | UPDATED  | [SourceForge](https://sourceforge.net/projects/lupesoltec-builds/files/daisy/ten/havoc/Havoc-OS-v3.3-20200405-2019-daisy-UNOFFICIAL.zip/download) |     ✔     |    ❌     |  ❌   |
| [Lineage OS 17.1](https://forum.xda-developers.com/mi-a2-lite/development/lineageos-17-1-xiaomi-a2-lite-t4076439) |      10.0       | [reujea0](https://forum.xda-developers.com/member.php?u=7249498) | UPDATED  | [AFH](https://androidfilehost.com/?fid=4349826312261762754)  |     ✔     |    ❌     |  ❌   |
| [Project Titanium](https://forum.xda-developers.com/mi-a2-lite/development/rom-projecttitanium-0-5-xiaomi-mi-a2-t4085665) |      10.0       | [ganomin](https://forum.xda-developers.com/member.php?u=9850043) | UPDATED  | [Mega](https://mega.nz/folder/CVU2hYTZ#yTD3p9cIGhp6WZdfHvXHmA) |     ✔     |    ✔     |  ✔   |

**📱Android 9.0 (Pie) Custom Roms**

|                           Rom Name                           |                          Developer                           |  Status  |                        Download link                         | Continues |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :------: | :----------------------------------------------------------: | :-------: |
|                            /e/ OS                            |                            FDoop                             | UPDATED  | [SourceForge](https://sourceforge.net/projects/fdoops-builds/files/eos/) |     ✔     |
| [Reloaded CAF](https://forum.xda-developers.com/mi-a2-lite/development/9-0-caf-reloaded-caf-mi-a2-lite-daisy-t3967509) | [tunasahin](https://forum.xda-developers.com/member.php?u=9165614) | Outdated | [Google Drive](https://drive.google.com/file/d/1q4te3ZkmVI_mCQ1yjRi6mkKekS944NGI/view?usp=drivesdk) |     ❌     |
| [Viper OS](https://forum.xda-developers.com/mi-a2-lite/development/9-0-viperos-v6-3-xiaomi-mi-a2-lite-t3927195) | [Opal06](https://forum.xda-developers.com/member.php?u=8661641) | Outdated | [Viper OS Web](https://download.viperos.org/?codename=daisy) |     ❌     |
| [AEX](https://forum.xda-developers.com/mi-a2-lite/development/9-0-aosp-extended-6-5-xiaomi-mi-a2-lite-t3930940) | [Acras01](https://forum.xda-developers.com/member.php?u=5583941) | Outdated | [Vanilla](https://drive.google.com/open?id=1MXZQr0jm1u3TWaoFKaAfP-1eRK0g7nj9) / [Gapps](https://drive.google.com/open?id=1BXduvu95Y6WKJqz8baVNSjSitxSFeYVF) |     ❌     |
| [AOSIP](https://forum.xda-developers.com/mi-a2-lite/development/9-0-aosip-rom-t3929596) | [Opal06](https://forum.xda-developers.com/member.php?u=8661641) | Outdated | [Sourgeforge](https://sourceforge.net/projects/aosip-daisy-ota/files/builds/) |     ❌     |
| [Resurrection Remix](https://forum.xda-developers.com/mi-a2-lite/development/9-0-resurrection-remix-v7-0-2-xiaomi-mi-t3926922) | [davidrocher](https://forum.xda-developers.com/member.php?u=8220724) | Outdated | [Resurrection Web](https://get.resurrectionremix.com/?dir=daisy) |     ❌     |
| [Lineage OS 16](https://forum.xda-developers.com/mi-a2-lite/development/lineageos-16-0-xiaomi-mi-a2-lite-t3919060) | [33bca](https://forum.xda-developers.com/member.php?u=5296790) | Outdated | [AFH](https://androidfilehost.com/?fid=6006931924117920053)  |     ❌     |
| [Superios OS](https://forum.xda-developers.com/mi-a2-lite/development/9-0-superioros-xiaomi-mi-a2-lite-t3946434) | [pawelik001](https://forum.xda-developers.com/member.php?u=8419529) | Outdated | [Sourceforge](https://sourceforge.net/projects/superioros/files/daisy/) |     ❌     |
| [barebones Los 16](https://forum.xda-developers.com/mi-a2-lite/development/rom-barebones-lineageos-16-0-t3931121) | [Eskuero](https://forum.xda-developers.com/member.php?u=4495609) | Outdated | [Github](https://github.com/Eskuero/patches_lineageos/releases) |     ❌     |
| [Paranoid Android](https://forum.xda-developers.com/mi-a2-lite/development/paranoid-android-pie-beta-xiaomi-mi-a2-t3912880) | [33bca](https://forum.xda-developers.com/member.php?u=5296790) | Outdated | [AFH](https://androidfilehost.com/?fid=1395089523397933724)  |     ❌     |
| [Evolution X](https://forum.xda-developers.com/mi-a2-lite/development/rom-evolution-x-2-0-t3942647) | [Zidan44](https://forum.xda-developers.com/member.php?u=9782219) | Outdated | [SourceForge](https://sourceforge.net/projects/evolution-x/files/daisy/) |     ❌     |
| [CypherOS](https://forum.xda-developers.com/mi-a2-lite/development/rom-cypheros-7-0-0-poundcake-unofficial-t3947303) | [Vitor00](https://forum.xda-developers.com/member.php?u=7824261) | Outdated | [Google Drive](https://drive.google.com/open?id=1ZtJPDbI1ZlB21iMdcGtMVXVFa5WrJ9-0) |     ❌     |

**📱Miui Ports**

| Android Version |     Rom Name      |                        Download Link                         |
| :-------------: | :---------------: | :----------------------------------------------------------: |
|       9.0       |     HybridOS      | [Google Drive](https://drive.google.com/file/d/16fWuD78AvNY6ugHMBVs0iMcUodKZI2bx/view) |
|       9.0       | Miui 11 V.11.0.4  | [Goolge Drive](https://drive.google.com/file/d/1M-eDCqCL2ypKLLa8uWAPOxzInWln5xAA/view) |
|       9.0       | Miui EU V11.0.5.0 | [Goolge Drive](https://drive.google.com/file/d/1-2htTk67oHQ9OuaKOxDWUJsiZDvs69sF/view) |



###### **📱Kernels**



|                         Kernel name                          | Supported Android Version | Linux Kernel Version |      Supported Roms       |                        Download Link                         |
| :----------------------------------------------------------: | :-----------------------: | :------------------: | :-----------------------: | :----------------------------------------------------------: |
| [Loki Caf](https://forum.xda-developers.com/mi-a2-lite/development/msm8953-c-f-4-9-kernel-t4033815) |           10.0            |         4.9          |        Custom Roms        |  [Github](https://github.com/Aarqw12/kernel_loki/releases/)  |
| [Chunchunmaru!](https://forum.xda-developers.com/mi-a2-lite/development/chunchunmaru-kernel-daisy-t4054441) |           10.0            |         4.9          |        Custom Roms        | [Google Drive](https://drive.google.com/file/d/17l7_jyHRAmVQv5PVsr68yXeWSPpD_Jj6/view) |
| [Nitrogen Kernel](https://forum.xda-developers.com/mi-a2-lite/development/r1-nitrogen-kernel-mi-a2-lite-t4011019) |           10.0            |       4.9.206        |        Custom Roms        | [AFH](https://www.androidfilehost.com/?w=files&flid=302702)  |
| [Kernel 3.18 CAF](https://forum.xda-developers.com/mi-a2-lite/development/kernel-daisy-q-oss-caf-rebase-fixed-t4067329) |           10.0            |         3.18         |         Stock Rom         | [Gtihub](https://github.com/Aarqw12/kernel_xiaomi_daisy-1/releases/) |
| [Dragonheart CAF](https://forum.xda-developers.com/mi-a2-lite/development/kernel-dragonheart-msm-caf-3-18-140-t3939288) |            9.0            |         3.18         |             ❔             |     [Yandex Disk](https://yadi.sk/d/b9IGc5vYXtMJdg/v3.3)     |
| [Revvz Kernel](https://forum.xda-developers.com/mi-a2-lite/development/v1-revvz-kernel-t4015937) |             ❔             |         4.9          |        Custom Roms        | [MediaFire](http://www.mediafire.com/file/x5gq5dn0ha09rpd/revvz_kernel_4.9_v1_daisy.zip/file) |
| [Butterfly](https://forum.xda-developers.com/mi-a2-lite/development/kernel-butterfly-daisy-t3919230) |         9.0 / 8.1         |         3.18         |    Custom / Stock Roms    |   [Yandex Disk](https://yadi.sk/d/B-T37WfMTR3hfA/v5.5%2B)    |
| [Genom CAF](https://forum.xda-developers.com/mi-a2-lite/development/genom-zero-kernel-daisy-t3900170) |        9.0  / 8.1         |       3.18.136       | Custom / Stock Roms / GSI |                                                              |
| [Thunderstorm](https://forum.xda-developers.com/mi-a2-lite/development/thunderstorm-zero-kernel-daisy-t3901924) |         9.0 / 8.1         |       3.18.135       | Custom / Stock Roms / GSI | [XDA](https://forum.xda-developers.com/attachment.php?attachmentid=4715368&stc=1&d=1551529439) |
| [Justice Kernel](https://forum.xda-developers.com/mi-a2-lite/development/ns-t3878678) |             ❔             |         3.18         |             ❔             | [Google Drive](https://drive.google.com/folderview?id=1v86GlzWDXpYq8V91LPdJBeodjuY8shYD) |



📱 **Stock Roms**

| Android Version |   Type   | Build number |                           Download                           | Recommented |
| :-------------: | :------: | :----------: | :----------------------------------------------------------: | :---------: |
|       10        |   OTA    |  V11.0.5.0   | [miui.com](https://bigota.d.miui.com/V11.0.5.0.QDLMIXM/miui_DAISYGlobal_V11.0.5.0.QDLMIXM_4e152f6600_10.0.zip) |      ❌      |
|       10        | Fastboot |  V11.0.4.0   | [miui.com](http://bigota.d.miui.com/V11.0.4.0.QDLMIXM/daisy_global_images_V11.0.4.0.QDLMIXM_20200404.0000.00_10.0_88297a8e92.tgz) |      ✔      |
|       9.0       | Fastboot |  V10.0.18.0  | [miui.com](http://bigota.d.miui.com/V10.0.18.0.PDLMIXM/daisy_global_images_V10.0.18.0.PDLMIXM_20200109.0000.00_9.0_a7b709656c.tgz) |      ✔      |
|       9.0       | Fastboot |  V10.0.17.0  | [miui.com](http://bigota.d.miui.com/V10.0.17.0.PDLMIXM/daisy_global_images_V10.0.17.0.PDLMIXM_20191209.0000.00_9.0_8a03587b9f.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.16.0  | [miui.com](http://bigota.d.miui.com/V10.0.16.0.PDLMIXM/daisy_global_images_V10.0.16.0.PDLMIXM_20191108.0000.00_9.0_8454506408.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.13.0  | [miui.com](http://bigota.d.miui.com/V10.0.13.0.PDLMIXM/daisy_global_images_V10.0.13.0.PDLMIXM_20190813.0000.00_9.0_5d0d486f04.tgz) |      ✔      |
|       9.0       | Fastboot |  V10.0.12.0  | [miui.com](http://bigota.d.miui.com/V10.0.12.0.PDLMIXM/daisy_global_images_V10.0.12.0.PDLMIXM_20190717.0000.00_9.0_59368ef014.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.10.0  | [miui.com](http://bigota.d.miui.com/V10.0.10.0.PDLMIXM/daisy_global_images_V10.0.10.0.PDLMIXM_20190613.0000.00_9.0_f1adc81d8e.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.9.0   | [miui.com](http://bigota.d.miui.com/V10.0.9.0.PDLMIXM/daisy_global_images_V10.0.9.0.PDLMIXM_20190514.0000.00_9.0_f9d0c739e0.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.7.0   | [miui.com](http://bigota.d.miui.com/V10.0.7.0.PDLMIXM/daisy_global_images_V10.0.7.0.PDLMIXM_20190318.0000.00_9.0_442d4f503e.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.3.0   | [miui.com](http://bigota.d.miui.com/V10.0.3.0.PDLMIXM/daisy_global_images_V10.0.3.0.PDLMIXM_20190114.0000.00_9.0_e8d8d4a6d0.tgz) |      ❌      |
|       9.0       | Fastboot |  V10.0.2.0   | [miui.com](http://bigota.d.miui.com/V10.0.2.0.PDLMIFJ/daisy_global_images_V10.0.2.0.PDLMIFJ_20181221.0000.00_9.0_fed1fcdf5d.tgz) |      ❌      |
|       8.1       | Fastboot |  V9.6.11.0   | [miui.com](http://bigota.d.miui.com/V9.6.11.0.ODLMIFF/daisy_global_images_V9.6.11.0.ODLMIFF_20181112.0000.00_8.1_8028e3bf1c.tgz) |      ✔      |
|       8.1       | Fastboot |  V9.6.10.0   | [miui.com](http://bigota.d.miui.com/V9.6.10.0.ODLMIFF/daisy_global_images_V9.6.10.0.ODLMIFF_20181030.0000.00_8.1_141b42a73d.tgz) |      ❌      |
|       8.1       | Fastboot |   V9.6.9.0   | [miui.com](http://bigota.d.miui.com/V9.6.9.0.ODLMIFF/daisy_global_images_V9.6.9.0.ODLMIFF_20181010.0000.00_8.1_baf840279a.tgz) |      ❌      |
|       8.1       | Fastboot |   V9.6.4.0   | [miui.com](http://bigota.d.miui.com/V9.6.4.0.ODLMIFF/daisy_global_images_V9.6.4.0.ODLMIFF_20180724.0000.00_8.1_4afd3431a2.tgz) |      ❌      |

**Custom Recovery**

|                             Name                             | Official |                           Download                           |                        Notes                         |
| :----------------------------------------------------------: | :------: | :----------------------------------------------------------: | :--------------------------------------------------: |
|                             TWRP                             |    ✔     | [Installer](https://eu.dl.twrp.me/daisy/twrp-installer-daisy-3.2.3-0.zip.html) / [Bootable](https://eu.dl.twrp.me/daisy/twrp-3.3.0-0-daisy.img.html) |              Can't install custom roms               |
|                         TWRP Offain                          |    ❌     | [Installer](https://github.com/TheDoop/daisy-stuff/raw/master/recovery/twrp-installer-daisy-3.3.1-0-offain.zip) / [Bootable](https://github.com/TheDoop/daisy-stuff/raw/master/recovery/twrp-daisy-3.3.1-0-offain.img) | Can't decrypt stock rom, USB MTP won't work A10 Roms |
|                      OrangeFox Recovery                      |    ✔     | [Installer](https://github.com/TheDoop/daisy-stuff/raw/master/recovery/OrangeFox-R10.0_2-Stable-daisy.zip) |                          -                           |
| [RedWolf Recovery](https://devotag.com/threads/redwolf-recovery-for-xiaomi-mi-a2-lite-daisy.766/) |    ❌     | [Bootable](https://github.com/TheDoop/daisy-stuff/raw/master/recovery/RedWolf.img) |                          -                           |
| [PitchBlack Recovery](https://devotag.com/threads/pitchblack-recovery-for-mi-a2-lite-daisy.762/) |    ❌     |                                                              |                          -                           |

**🔧Tools** 

|        Tool         |  Version   |                           Download                           |
| :-----------------: | :--------: | :----------------------------------------------------------: |
| Mi Flash tool amd64 | 2016.04.01 | [Github](https://raw.githubusercontent.com/TheDoop/daisy-stuff/master/tools/MiPhone.exe) |
|    Mi Flash tool    |   7.4.25   | [Github](https://github.com/TheDoop/daisy-stuff/raw/master/tools/MiFlashSetup_eng.msi) |
|        QPTS         |  2.7.480   | [Github](https://github.com/TheDoop/daisy-stuff/raw/master/tools/QPST.zip) |
|        QFIL         |  2.0.2.3   | [Gtihub](https://github.com/TheDoop/daisy-stuff/raw/master/tools/QFIL.zip) |

